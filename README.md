# image2palette

A command line script that generates a flexible-width html file
(and a small jpeg) displaying a palette of colours based on a user
selected image file. Files are saved in the directory of the
original image.

Requires installation of numpy and PIL.

## Example Output

![example 2](example_output/example_2.png)
![example 1](example_output/example_1.png)