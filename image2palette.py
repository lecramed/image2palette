# image2palette.py
# v1.0
#
# A command line script that generates a flexible-width html file
# (and small jpeg) displaying a palette of colours based on a user
# selected image file. Files are saved in the directory of the
# original image.
#
# Requires that numpy and PIL are installed.

# @TODO: make code error-safe
# @TODO: implement an algorithm that accounts for relative
# difference between colour channels - see NOTE1 at colourSim().


##########
# imports
##########
import os
from math import sqrt
from statistics import mean
import numpy as np
from PIL import Image
import tkinter as tk
from tkinter.filedialog import askopenfilename


# Remove the annoying GUI we are not actually using to select the file.
root = tk.Tk()
root.withdraw()


def main():
    ##########
    # minor functions
    ##########
    def clamp256(x):
        '''
        Truncates a number at the boundaries 0 and 255.
        '''
        # We cannot allow values outside the unsigned 8 bit range.
        return max(0, min(x, 255))

    def rgb2hex(r, g, b):
        '''
        Converts red, green, and blue rgb values to six digit hex.
        Requires clamp256.
        '''
        return ("#{0:02x}{1:02x}{2:02x}".format(clamp256(r), clamp256(g), clamp256(b)))

    def subtract255(x, y):
        '''
        Subtracts two uint8 (0-255) values such that the result is never negative.
        '''
        # Required to maintain unsigned 8 bit values.
        if x >= y:
            return x - y
        else:
            return y - x

    def luminance(r, g, b):
        '''
        Calculates the "HSP" perceived brightness value of an rgb triplet.
        '''
        # Requires sqrt from math.
        return sqrt(0.299 * r**2 + 0.587 * g**2 + 0.114 * b**2)

    def colourSim(r1, g1, b1, r2, g2, b2):
        '''
        Computes the arithmetic similarity between each component
        of two sets of rgb colours, then uses that to return an overall
        similarity value.
        Requires subtract255.
        '''
        # Requires mean from statistics.

        # NOTE1: THIS EXCLUDES EVEN VERY DIFFERENT COLOURS AS IMPLEMENTED.
        # This is because the r, g, and b results are not compared
        # one to another before the mean is calculated.

        # Calculate the differences.
        # Uses "255 -" to make large values = more similar.
        red_diff = 255 - subtract255(r1, r2)
        green_diff = 255 - subtract255(g1, g2)
        blue_diff = 255 - subtract255(b1, b2)

        # Make percentage values.
        red_per = red_diff / 255
        green_per = green_diff / 255
        blue_per = blue_diff / 255

        # Compute the similarity value.
        return mean([red_per, green_per, blue_per])

    ##########
    # functions
    ##########
    def asVoid(arr):
        '''
        View the array as dtype np.void (bytes). This collapses ND-arrays
        to 1D-arrays, so you can perform 1D operations on them.
        Warning:
        >>> asVoid([-0.]) == asVoid([0.])
        array([False], dtype=bool)
        '''
        # reference:
        # http://stackoverflow.com/a/16216866/190597 (Jaime)
        # http://stackoverflow.com/a/16840350/190597 (Jaime)
        arr = np.ascontiguousarray(arr)
        return arr.view(np.dtype((np.void, arr.dtype.itemsize * arr.shape[-1])))

    def paletteDescending(img):
        '''
        Return palette in descending order of frequency.
        '''
        # Requires numpy.
        arr = np.asarray(img)
        # Get the unique colours and their indexes.
        palette, index = np.unique(asVoid(arr).ravel(), return_inverse=True)
        palette = palette.view(arr.dtype).reshape(-1, arr.shape[-1])
        # Get the number of times a colour is used.
        count = np.bincount(index)
        # Sort by frequency.
        order = np.argsort(count)
        return palette[order[::-1]]

    ##########
    # user input and parsing
    ##########
    print('')
    print('Select the image to palettize.')
    print('')
    imagefile = askopenfilename()

    # Generate the save location in the same directory.
    save_path = os.path.splitext(imagefile)[0]
    image_save_path = save_path + "_small.jpg"
    # Get the filenames required for html.
    filename = os.path.basename(imagefile).split('.')[0]
    full_filename = os.path.basename(imagefile)
    small_filename = filename + "_small.jpg"

    # User sets the value above which similar colours will be discarded.
    print('Input the "sameness" cutoff value. This is a number')
    print('between 0 and 1, where 1 allows the most similar colours.')
    sameness_cutoff = float(input('(A good starting value might be 0.8): '))

    ##########
    # create palette from image
    ##########
    # Open the image.
    img = Image.open(imagefile, 'r').convert('RGB')

    # Resize the image for speed and binning.
    # Specify the maximum dimension in pixels.
    max_length = 90
    img_width, img_height = img.size
    resize_ratio = min(max_length/img_width, max_length/img_height)
    img_resize = int(resize_ratio * img_width), int(resize_ratio * img_height)
    img_s = img.resize(img_resize)

    # Save in image directory.
    img_s.save(image_save_path, "jpeg")

    # Generate the palette array.
    palette = paletteDescending(img_s)  # use (img_s)[:10] to get the top ten

    ##########
    # remove colours that are overly similar
    ##########
    # The check array is initialized to False to skip an initial duplicate entry.
    check = [False]
    # The colour being checked is initialized to the first palette colour.
    check_colour = palette[:1]
    # The array of dissimilar colours is initialized to the first palette colour.
    palette_dissimilar = palette[:1]

    # Check every entry in the palette array to see if it should be removed
    # based on how similar it is to the existing entries in the
    # palette_dissimilar array. If it is different enough, add it to the
    # palette_dissimilar array and continue looping through the full palette.
    for row_p in palette:
        # If the current colour results in a check array with only True values,
        # append it to the dissimilar colours array.
        if all(check):
            palette_dissimilar = np.vstack((palette_dissimilar, check_colour))
        # Reinitialize check list for every new colour.
        check = []
        # Set the current colour being checked.
        check_colour = row_p
        # Populate the check array by comparing the current colour to the array
        # of dissimilar colours based on the user-specified cutoff value.
        for row_k in palette_dissimilar:
            if colourSim(row_k[0], row_k[1], row_k[2], check_colour[0], check_colour[1], check_colour[2]) < sameness_cutoff:
                check.append(True)
            else:
                check.append(False)

    # Print the final array for copy/paste purposes.
    print('')
    print('The {} colour palette is:'.format(len(palette_dissimilar)))
    print()
    print(palette_dissimilar)

    ##########
    # calculate the palette colour brightness and use a contrasting text colour
    ##########
    # Initialize an empty text colour array.
    text_colour = []
    for row in palette_dissimilar:
        luma = luminance(row[0], row[1], row[2])
        if luma < 128:
            text_colour.append('#f0f0f0')
        else:
            text_colour.append('#000000')

    ##########
    # print html
    ##########
    # Set the header.
    # Note the annoying double {{ to allow the format string.
    html_header = '''<!doctype html>
    <html lang="en">
        <head>
            <meta charset="utf-8">
            <title>autoPalette</title>
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <style>
             #container {{
                 display: inline-block;
                 justify-content: center;
                 background-color: #dddddd;
                 width: 100%;
             }}
             #contentbox {{
                 display: flex;
                 justify-content: flex-start;
                 text-align: center;
                 font-family: monospace;
                 flex-wrap: wrap;
                 background-color: #888888;
                 width: 100%;
             }}
             header {{
                 display: flex;
                 justify-content: space-between;
                 align-items: center;
                 padding: 10px;
             }}
             .logo {{
                 font-family: monospace;
                 color: #145f7d;
                 padding: 0em 0em 0em 1em;
             }}
             .left {{
                 float: left;
             }}
             .right {{
                 float: right;
             }}
             img {{
                 max-width: {0}px;
                 max-height: {0}px;
                 padding: 10px;
             }}
    '''.format(max_length)

    # *** css rectangle definitions go here ***

    # Set the intermediate sections (mind the closing tags).
    html_mid1 = '''</style>
      </head>
      <body>
        <div id="container">
          <div id="header">
            <header>
              <span class="left">
                <h1 class="logo">palette from: {0}</h1>
              </span>
              <span class="right">
                <h1 class="logo" style="color: #6f2e34;">colour sameness: {1:.0f}%</h1>
              </span>
              <img src="{2}">
            </header>
          </div>

          <div id="contentbox">
    '''.format(full_filename, sameness_cutoff * 100, small_filename)

    # *** html rectangle divs go here ***

    # Set the footer (mind the closing tags).
    html_footer = '''</div>
        </div>
      </body>
    </html>
    '''

    # Set the div rectangle size.
    r_width = 90
    r_height = 90

    # Generate and save the html output.
    with open(save_path + '_autoPalette.html', 'w') as f:
        # Print the header.
        f.write(html_header)
        # Print the palette box css.
        for i, row in enumerate(palette_dissimilar):
            f.write("#r{} {{\n".format(i))
            f.write("padding: 0.5em;\n")
            f.write("width: {}px;\n".format(r_width))
            f.write("height: {}px;\n".format(r_height))
            f.write("color: {};\n".format(text_colour[i]))
            f.write("background: {};\n".format(rgb2hex(row[0], row[1], row[2])))
            f.write("}\n")
        # Print some tags and the explanatory text.
        f.write(html_mid1)
        # Print the palette box html.
        for i, row in enumerate(palette_dissimilar):
            f.write('<div id="r{0}" title="{1}">{1}<br>{2},{3},{4}</div>\n'.format(i, rgb2hex(row[0], row[1], row[2]), row[0], row[1], row[2]))
        # Print the footer.
        f.write(html_footer)


if __name__ == '__main__':
    main()
